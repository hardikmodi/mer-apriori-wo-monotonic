/* 
 * File:   SetOfSet.cpp
 * Author: aeon
 * 
 * Created on 19 December, 2012, 10:03 PM
 */

#include<iostream>
#include "SetOfSet.h"

bool SetOfSet::subsetCheck(set<int> &x)
{
    set<int>::iterator x_itr = x.begin();
    set<set<int> >::iterator s_itr ;
    for (s_itr = s.begin(); s_itr!=s.end();s_itr++)
    {
        long int count=0;
        set<int>::iterator y_itr= (*s_itr).begin();
        while((y_itr!=(*s_itr).end()) && (x_itr!=x.end()))
        {
            if ((*y_itr) == (*x_itr))
            {
                x_itr++;
                y_itr++;
                count++;
            }
            else
            {
                if ((*y_itr)<(*x_itr))
                    y_itr++;
                else
                    x_itr++;
            }
        }
        if (count==x.size())
            return (true);
    }
    return (false);                     
}
bool SetOfSet::add(set<int> x)
{
    return (s.insert(x)).second;
}

bool SetOfSet::isIntersection(const set<int> &a, const set<int> &b)
{
    set<int>::iterator a_itr,b_itr;
    a_itr = a.begin();
    b_itr = b.begin();
    while ((a_itr != a.end()) && (b_itr != b.end()))
    {
        if ((*a_itr) == (*b_itr))
            return true;
        else
        {
            if (*a_itr < *b_itr)
                a_itr ++;
            else
                b_itr ++;
        }
    }
    return false;
}

long int SetOfSet::cardinality(void)
{
    return s.size();
}

set< set<int> >::iterator SetOfSet::first(void)
{
    return s.begin();
}

set< set<int> >::iterator SetOfSet::last(void)
{
    return s.end();
    
}

void SetOfSet::display(void)
{
    set< set<int> >::iterator itr;
    for (itr=s.begin();itr!=s.end();itr++)
    {
        set<int>::iterator it;
        for(it=(*itr).begin();it!=(*itr).end();it++)
        {    
            cout<<(*it)<<", ";
        }
        cout<<endl;
    }
}