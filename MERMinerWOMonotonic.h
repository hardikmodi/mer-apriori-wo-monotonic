/* 
 * File:   BruteMiner.h
 * Author: aeon
 *
 * Created on 7 January, 2013, 10:51 PM
 */

#include "SetOfSet.h"
#include <string>
#include <map>
#include "Dataset.h"

using namespace std;

#ifndef BRUTEMINER_H
#define	BRUTEMINER_H

class MERMinerWOMonotonic {
public:
     private:
        Dataset &ds;        
        SetOfSet Freq;
        float minsup;
        float minconf;
        long int minsupportcount;
        string output_file_path;            
        set<int> candidate,X,Y;
        map<set<int> , float> FreqSupp;
        void getNextCandidateItemset(void);
        float getSupportCount(void);
        void getNextRule(void);
        float calculateConfidence(void);
        long int writeRuleToFile(ofstream& out, set<int> a,set<int> b);
        long int writeRuleToFile(ofstream& out, set<int> a,set<int> b,float confval);
    public:
        MERMinerWOMonotonic(Dataset &d);
        long int mine(char  *o_path,float m_sup,float m_conf);             
        void showRule(void); 
        clock_t timeGetNextCandidateItemset, timeGetSupportCount, timeGetNextRule, timeCalculateConfidence, timeSubsetCheck,timePhase1;
        long int countGetNextCandidateItemset, countGetSupportCount, countGetNextRule, countCalculateConfidence;

};

#endif	/* BRUTEMINER_H */

